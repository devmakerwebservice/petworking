$.get('/graph', function (data) {
    var meses= ["Janeiro","Fevereiro","Marco","Abril","Maio","Junho","Julho","Agosto","Septembro","Outubro","Novembro","Dezembro"];
    
    var lineData = {
        labels: meses,
        datasets: [
            {
                fillColor: "rgba(247,129,0,0.5)",
                strokeColor: "rgba(247,129,0,0.5)",
                data: [
                    data[0].jan,
                    data[0].fev,
                    data[0].mar,
                    data[0].abr,
                    data[0].mai,
                    data[0].jun,
                    data[0].jul,
                    data[0].ago,
                    data[0].set,
                    data[0].out,
                    data[0].nov,
                    data[0].dez
                ],
            }
        ]
    };

    var lineOptions = {
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.1)",
        scaleGridLineWidth: 1,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: false,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        responsive: true
    };


    var ctx = document.getElementById("lineChart").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
})