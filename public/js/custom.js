function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#avatar')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function bannerURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#foto')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$("#cep").blur(function() {
    var cep = $(this).val().replace(/\D/g, '');
    if (cep != "") {
        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#address").val(dados.logradouro);
                $("#city").val(dados.localidade);
                $("#state").val(dados.uf);
            }
        });
    }
});

$('input[name=banner]').on('change', function() {
    if($(this).val() == 't') {
        $(".pic").toggle(true)
    }else{
        $(".pic").toggle(false)
    }
});

$("#passwordfield").on("keyup",function(){
    if($(this).val())
        $(".glyphicon-eye-open").show();
    else
        $(".glyphicon-eye-open").hide();
});
$(".glyphicon-eye-open").mousedown(function(){
    $("#passwordfield").attr('type','text');
}).mouseup(function(){
    $("#passwordfield").attr('type','password');
}).mouseout(function(){
    $("#passwordfield").attr('type','password');
});

function view_pet(id) {
    $.get("/pet/view/" + id)
        .done(function (data) {
            console.log(data);
            if (data.success){
                $('.modal-body').html(
                    '<div class="row text-center">'+
                        '<div class="col-xs-3 col-xs-offset-1">' +
                            '<img src="' + data.pet.foto  + '" style="height: 100px" alt="" class="img-circle">' +
                        '</div>' +
                        '<div class="col-xs-7 text-left">' +
                            '<p class="pet-name"><b>Nome do animal: </b>'+data.pet.name+'</p>' +
                            '<p class="pet-name"><b>Localização: </b>'+data.pet.city +'/'+ data.pet.state+' - '+data.pet.neighborhood+'</p>' +
                            '<p class="pet-name"><b>Dono: </b>'+data.pet.dono +'</p>' +
                        '</div>' +
                    '</div>'
                );
                $('#showModal').modal('show')
            }
        })
}

function view_partner(id) {
    $.get("/parceiro/view/" + id)
        .done(function (data) {
            console.log(data);
            if (data.success){
                $('.modal-body').html(
                    '<div class="row text-center">'+
                        '<div class="col-xs-4" style="display:flex;align-items:center;height:120px;">' +
                            '<img src="' + data.ptn.path_banner  + '" style="height: 100px" alt="">' +
                        '</div>' +
                        '<div class="col-xs-8 text-left">' +
                            '<p class="pet-name"><b>Nome: </b>'+data.ptn.name+'</p>' +
                            '<p class="pet-name"><b>Localização: </b>'+data.ptn.address+' '+data.ptn.number+' - '+data.ptn.city+'/'+data.ptn.state+'</p>' +
                            '<p class="pet-name"><b>Categoria: </b>'+data.ptn.category +'</p>' +
                        '</div>' +
                    '</div>'
                );
                $('#showModal').modal('show')
            }
        })
}

function delete_admin(id) {
    swal({
        title: "Excluir",
        text: "Você deseja deletar este administrador?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E23773",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function() {
        $.get("/admin/delete/" + id)
            .done(function (data) {
                if (data.success == true) {
                    swal({
                        title: "Excluir",
                        text: "Administrador deletado com sucesso",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#E23773",
                        confirmButtonText: "OK"
                    },
                    function () {
                        location.reload();
                    });
                }else{
                    swal({
                        title: "Excluir",
                        text: "Não possível deletar este administrador.",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 2500,
                    });
                }
            });
    });
}

function delete_partner(id) {
    swal({
        title: "Excluir",
        text: "Você deseja deletar este parceiro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E23773",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function() {
        $.get("/parceiro/delete/" + id)
            .done(function (data) {
                if (data.success == true) {
                    swal({
                        title: "Excluir",
                        text: "Parceiro deletado com sucesso",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#E23773",
                        confirmButtonText: "OK"
                    },
                    function () {
                        location.reload();
                    });
                }else{
                    swal({
                        title: "Excluir",
                        text: "Não possível deletar este parceiro.",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 2500,
                    });
                }
            });
    });
}

function delete_tip(id) {
    swal({
        title: "Excluir",
        text: "Você deseja deletar esta dica?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E23773",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function() {
        $.get("/dica/delete/" + id)
            .done(function (data) {
                if (data.success == true) {
                    swal({
                        title: "Excluir",
                        text: "Dica deletada com sucesso",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#E23773",
                        confirmButtonText: "OK"
                    },
                    function () {
                        location.reload();
                    });
                }else{
                    swal({
                        title: "Excluir",
                        text: "Não possível deletar esta dica.",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 2500,
                    });
                }
            });
    });
}

function check_tip(id) {
    swal({
        title: "Aprovar",
        text: "Você deseja aprovar esta dica?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E23773",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function() {
        $.get("/dica/aprov/" + id)
            .done(function (data) {
            if (data.success == true) {
                swal({
                        title: "Aprovar",
                        text: "Dica aprovada com sucesso",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#E23773",
                        confirmButtonText: "OK"
                    },
                    function () {
                        location.reload();
                    });
            }else{
                swal({
                    title: "Aprovar",
                    text: "Não possível aprovar esta dica.",
                    type: 'error',
                    showConfirmButton: false,
                    timer: 2500,
                });
            }
        });
    });
}

function delete_pet(id) {
    swal({
        title: "Excluir",
        text: "Você deseja deletar este animal?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E23773",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function() {
        $.get("/pet/delete/" + id)
            .done(function (data) {
                if (data.success == true) {
                    swal({
                        title: "Excluir",
                        text: "Animal deletado com sucesso",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#E23773",
                        confirmButtonText: "OK"
                    },
                    function () {
                        location.reload();
                    });
                }else{
                    swal({
                        title: "Excluir",
                        text: "Não possível deletar este animal.",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 2500,
                    });
                }
            });
    });
}

function delete_user(id) {
    swal({
        title: "Excluir",
        text: "Você deseja deletar este usuário?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E23773",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    },
    function() {
        $.get("/user/delete/" + id)
            .done(function (data) {
                if (data.success == true) {
                    swal({
                        title: "Excluir",
                        text: "Usuário deletado com sucesso",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#E23773",
                        confirmButtonText: "OK"
                    },
                    function () {
                        location.reload();
                    });
                }else{
                    swal({
                        title: "Excluir",
                        text: "Não possível deletar este usuário.",
                        type: 'error',
                        showConfirmButton: false,
                        timer: 2500,
                    });
                }
            });
    });
}

$('#type').on('change', function () {
    var city = $('#city');
    var state = $('#state');

    if($(this).val() === 'state')
    {
        state.toggle(true);
        city.toggle(false);
        city.children().attr('disabled',true);
    }
    else if($(this).val() === 'city')
    {
        state.toggle(true);
        city.toggle(true);
        city.children().attr('disabled',false);
    }
    else {
        state.toggle(false);
        city.toggle(false);
        city.children().attr('disabled',true);
        $("[name=estado]").val(0);
    }
})