<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('facebook_id')->unique()->nullable();
            $table->string('phone')->nullable();
            $table->string('path_foto')->nullable();
            $table->string('status')->default(0);
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('role');
            $table->string('device_token')->nullable();
            $table->string('device_model')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}