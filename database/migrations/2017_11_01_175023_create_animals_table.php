<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('species');
            $table->string('other_specie')->nullable();
            $table->tinyInteger('size');
            $table->string('age')->nullable();
            $table->string('genre', 1);
            $table->string('color')->nullable();
            $table->string('breed')->nullable();
            $table->date('date_lost')->nullable();
            $table->date('date_find')->nullable();
            $table->string('others_info')->nullable();
            $table->string('state');
            $table->string('city');
            $table->string('neighborhood');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            /*$table->integer('age_id')->unsigned()->nullable();
            $table->foreign('age_id')->references('id')->on('ages');*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
