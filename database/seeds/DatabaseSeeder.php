<?php

use App\Entities\Ages;
use App\Entities\User;
use App\Entities\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('categories')->insert(array(
		    0 =>
            array(
                'id' => 1,
                'name' => 'Veterinárias',
                'created_at' => '2018-03-15 15:08:00',
                'updated_at' => '2018-03-15 15:08:00'
            ),
            1 =>
            array(
                'id' => 2,
                'name' => 'Hotéis',
                'created_at' => '2018-03-15 15:08:00',
                'updated_at' => '2018-03-15 15:08:00'
            ),
            2 =>
            array(
                'id' => 3,
                'name' => 'Petshops',
                'created_at' => '2018-03-15 15:08:00',
                'updated_at' => '2018-03-15 15:08:00'
            ),
            3 =>
            array(
                'id' => 4,
                'name' => 'Adestradores',
                'created_at' => '2018-03-15 15:08:00',
                'updated_at' => '2018-03-15 15:08:00'
            ),
            4 =>
            array(
                'id' => 5,
                'name' => 'ONGS',
                'created_at' => '2018-03-15 15:08:00',
                'updated_at' => '2018-03-15 15:08:00'
            )
		));

		DB::table('users')->insert(array(
		    0 =>
            array(
		        'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@petworking.com',
                'password' => bcrypt('123'),
                'role' => 'admin',
                'created_at' => '2018-03-15 15:08:00',
                'updated_at' => '2018-03-15 15:08:00'
            )
        ));
    }
}
