<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Entities\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '123',
        'phone' => $faker->phoneNumber,
        'path_foto' => $faker->imageUrl($width = 640, $height = 480),
        'role' => 'user',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Entities\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});

$factory->define(App\Entities\Partners::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'category_id' => \App\Entities\Category::all()->random()->id
    ];
});