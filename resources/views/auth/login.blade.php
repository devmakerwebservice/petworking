@extends('layouts.app')


@section('content')
    <div>
        <div>

            <img src="{{ asset('img/logo_login.png') }}" style="width: 200px" alt="">

        </div>
        <center>
            <form class="m-t" role="form" action="{{ route('login') }}" method="POST" style="width: 70%">
                {{ csrf_field() }}
                <div class="form-group" id="email">
                    <input id="email" type="email" class="form-control" name="email" placeholder="EMAIL" required autofocus>
                </div>
                <div class="form-group" id="email">
                    <input type="password" name="password" class="form-control" ame="password" placeholder="SENHA">
                </div>
                <div class="pull-left" style="text-decoration: underline;color: #000;margin-bottom: 20px;">
                    <input type="checkbox" name="remember" style="margin:0 5px 0 0;"><small>Lembrar-me</small>
                </div>
                {{--<a href="{{ route('password.request') }}" class="esqueci-senha pull-right" style="text-decoration: underline; color: #000; font-size: 14px; margin-bottom: 20px">--}}
                {{--<small>Esqueci minha senha</small>--}}
                </a>

                <button type="submit" class="btn btn-primary block full-width">Entrar</button>

            </form>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </center>
    </div>
@endsection
