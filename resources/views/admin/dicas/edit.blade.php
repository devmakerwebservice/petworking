@extends('admin.layout.dashboard')

@section('styles-head')
    <style>
        .input-user {
            position: relative;
            outline: 0;
            border: 0;
            border-bottom: 1px solid #D1D1D1;
            padding:0;
        }
        .input-user:focus {
            border-color: #E23773 !important;
        }
    </style>
@endsection

@section('title')
    Edição de Dica
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Edição de Dica
                    </h4>
                </div>
                <div class="ibox-content">
                    <div class="row" style="margin-top: 20px">
                        <div class="col-lg-12 text-center">
                            <form class="form-horizontal" method="POST" action="{{ url('/dica/editar/'.$dica->id) }}" style="margin: auto">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $dica->id }}">
                                <div class="form-group">
                                    <div class="col-lg-12 text-left">
                                        <label for="not" class="">Descrição</label>
                                        <textarea name="description" class="form-control" style="min-height: 200px; max-width: 100%">{{ $dica->description }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 col-lg-offset-6 text-right">
                                        <button type="submit" class="btn btn-primary" style="padding: 6px 35px;">
                                            Enviar
                                            <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts-footer')

@endsection
