@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Lista de Usuários
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Lista de usuários cadastrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    @include('admin.lista.table.index')
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts-footer')
    <script>
        var oTable = $('#users-table').DataTable({
            bLengthChange: false,
            bSort : true,
            bInfo : false,
            bFilter: true,
            processing: false,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0,1,2,3],
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ],
            ajax: {
                "url": '{!! route('lista.table') !!}',
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email', orderable: false},
                { data: 'phone', name: 'phone', orderable: false},
                { data: 'pet', name: 'pet'},
                { data: 'delete', name: 'delete', orderable: false}
            ]
        });

        document.querySelector('[aria-controls="users-table"]').className += ' excel';
    </script>
@endsection
