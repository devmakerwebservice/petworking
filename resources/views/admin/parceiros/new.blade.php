@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Cadastro de Parceiro
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Cadastrar novo Parceito
                    </h4>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="{{ url('parceiro/new') }}" style="margin: auto" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-4 col-lg-offset-4 text-left">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="name">Nome</label>
                                        <input type="text" id="name" name="name" class="form-control" tabindex="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="category">Categoria</label>
                                        <select name="category_id" class="form-control" id="category" tabindex="2">
                                            <option></option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="tel">Telefone</label>
                                        <input type="text" id="tel" name="phone" class="form-control" tabindex="3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="cep">CEP</label>
                                        <input type="text" id="cep" name="cep" class="form-control" tabindex="4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="address">Endereço</label>
                                        <input type="text" id="address" name="address" class="form-control" tabindex="5">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="num">Número</label>
                                        <input type="text" id="num" name="number" class="form-control" tabindex="6">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="comp">Complemento</label>
                                        <input type="text" id="comp" name="comp" class="form-control" tabindex="7">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="state">Estado</label>
                                        <input type="text" id="state" name="state" class="form-control" tabindex="9">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="city">Cidade</label>
                                        <input type="text" id="city" name="city" class="form-control" tabindex="8">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label for="banner">Este parceiro possuí banner?</label><br>
                                        <input type="radio" id="banner" value="t" name="banner"/>Sim
                                        <input type="radio" id="banner" value="f" name="banner" checked/>Não
                                    </div>
                                </div>
                                <div class="form-group pic" style="display: none;">
                                    <div class="col-lg-12 text-center">
                                        <label class="btn-bs-file btn" style="margin-top: 25px">
                                            <img src="{{ asset('img/banner.png') }}" style="width: 100%;height: 150px" id="foto" alt="Profile picture">
                                            <input type="file" id="image-input" name="path_banner" onchange="bannerURL(this);" accept="image/*"/>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 text-right">
                                        <button type="reset" class="btn btn-danger" style="padding: 6px 35px;">Cancelar</button>
                                    </div>
                                    <div class="col-lg-6 text-left">
                                        <input type="submit" value="Salvar Cadastro" class="btn btn-primary"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts-footer')

@endsection
