@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Lista de Parceiros
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Lista de parceiros cadastrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    @include('admin.parceiros.table.index')
                </div>
            </div>
        </div>
    </div>

    @include('admin.includes.modal')

@endsection


@section('scripts-footer')
    <script>
        var oTable = $('#partner-table').DataTable({
            bLengthChange: false,
            bSort : true,
            bInfo : false,
            bFilter: true,
            processing: false,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    text: 'Cadastrar',
                    action: function () {
                        window.location = '{!! url('/parceiro/cadastro') !!}';
                    },
                    className: 'new'
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [1,2,3,4,5],
                        modifier: {
                            page: 'current'
                        }
                    },
                    className: 'excel'
                }
            ],
            ajax: {
                "url": '{!! route('parceiros.table') !!}',
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'banner', name: 'banner', orderable: false},
                { data: 'name', name: 'name'},
                { data: 'phone', name: 'phone'},
                { data: 'local', name: 'local', orderable: false},
                { data: 'category', name: 'category'},
                { data: 'view', name: 'view', orderable: false},
                { data: 'editar', name: 'editar', orderable: false},
                { data: 'delete', name: 'delete', orderable: false}
            ]
        });

        document.querySelector('[aria-controls="partner-table"]').className += ' excel';
    </script>
@endsection
