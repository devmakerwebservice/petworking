@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Editar Usuário
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Editar minha conta
                    </h4>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="{{ url('user/editar') }}" style="margin: auto" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row text-center">
                            <img src="{{ $data->path_foto }}" style="box-shadow: 0 0 30px #afafaf" class="img-circle avatar" id="avatar" alt="Profile picture">
                            <br>
                            <label class="btn-bs-file btn btn-info" style="margin-top: 25px">
                                Alterar Foto
                                <input type="file" id="image-input" value="" name="foto" onchange="readURL(this);" accept="image/*">
                            </label>
                        </div>

                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-6 col-lg-offset-3 text-center">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" placeholder="Nome" name="nome" value="{{ $data->name }}" class="form-control input-user" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="email" placeholder="Email" name="email" value="{{ $data->email }}" class="form-control input-user" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="password" name="atual" placeholder="Senha Atual" style="padding-right: 30px;" class="form-control input-user" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="password" id="passwordfield" name="senha" placeholder="Senha" style="padding-right: 30px;" class="form-control input-user icon">
                                        <span class="glyphicon glyphicon-eye-open" style="display:none;right: 15px;position: absolute;top: 12px;cursor:pointer;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="password" placeholder="Confirmar Senha" name="senha_confirmation" class="form-control input-user">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <a onclick="destroy()">Excluir minha conta</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 text-right">
                                        <button type="button" class="btn btn-danger" style="padding: 6px 35px;">Cancelar</button>
                                    </div>
                                    <div class="col-lg-6 text-left">
                                        <button type="submit" class="btn btn-primary">Salvar Cadastro</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts-footer')
    <script>
        function destroy(){
            swal({
                title: "Excluir",
                text: "Você deseja deletar seu usuário?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E23773",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false
            },
            function() {
                swal({
                    title: "Deletar",
                    text: "Usuário deletado com sucesso",
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500,
                });
            });
        }
    </script>
@endsection
