@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Cadastro novo admin
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Cadastrar novo Admin
                    </h4>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="{{ url('admin/new') }}" style="margin: auto" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row text-center">
                            <img src="{{ asset('img/perfil.png') }}" class="img-circle avatar" id="avatar" alt="Profile picture">
                            <br>
                            <label class="btn-bs-file btn btn-info" style="margin-top: 25px">
                                Alterar Foto
                                <input type="file" id="image-input" name="path_foto" onchange="readURL(this);" accept="image/*"/>
                            </label>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-6 col-lg-offset-3 text-center">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" placeholder="Nome" name="name" class="form-control input-user">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="email" placeholder="Email" name="email" class="form-control input-user">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="password" id="passwordfield" name="password" placeholder="Senha" style="padding-right: 30px;" class="form-control input-user icon">
                                        <span class="glyphicon glyphicon-eye-open" style="display:none;right: 15px;position: absolute;top: 12px;cursor:pointer;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="password" placeholder="Confirmar Senha" name="password_confirmation" class="form-control input-user">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 text-right">
                                        <button type="reset" class="btn btn-danger" style="padding: 6px 35px;">Cancelar</button>
                                    </div>
                                    <div class="col-lg-6 text-left">
                                        <input type="submit" value="Salvar Cadastro" class="btn btn-primary"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts-footer')
    <script>
        function create(){
            swal({
                title: "Cadastro Admin",
                text: "Admin cadastrado com sucesso",
                type: 'success',
                showConfirmButton: false,
                timer: 3000,
            });
        }
    </script>
@endsection
