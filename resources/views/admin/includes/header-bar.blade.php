<nav class="navbar navbar-static-top white-bg text-center" role="navigation" style="margin-bottom: 0;padding: 10px 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
    </div>

    <img alt="image" src="{{ asset('img/logo_petworking.png') }}" style="width: 200px;padding-top: 5px"/>

    <ul class="nav navbar-top-links navbar-right">
        <li>
            <div class="">
                <img alt="image" class="img-circle header-avatar" src="{{ Auth::user()->path_foto }}"/>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="block m-t-xs" style="float:left;color: #000; margin-top: 20px">
                        <strong class="font-bold">{{ Auth::user()->name }}</strong>
                        <b class="caret"></b>
                    </span>
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li>
                        <a href="{{ url('/user') }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            Editar minha conta
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/admin') }}">
                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                            Cadastrar novo admin
                        </a>
                    </li>
                    <li>
                        <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            Sair
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</nav>