<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a href="{{ url('/') }}" >
                        <img alt="image" src="{{ asset('img/logo.png') }}" style="width: 200px"/>
                    </a>
                </div>
                <div class="logo-element">
                    <img alt="image" src="{{ asset('img/logo_login.png') }}" style="width: 70px"/>
                </div>
            </li>
            <li class="{{ isset($page) && $page == "dashboard" ? 'active' : '' }}">
                <a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span>  </a>
            </li>
            <li class="{{ isset($page) && $page == "cadastros" ? 'active' : '' }}">
                <a href="{{ url('/cadastros') }}"><i class="fa fa-paw"></i> <span class="nav-label">Lista de Pets</span></a>
            </li>
            <li class="{{ isset($page) && $page == "encontrados" ? 'active' : '' }}">
                <a href="{{ url('/encontrados') }}"><i class="fa fa-exclamation-circle"></i> <span class="nav-label">Pets Econtrados</span></a>
            </li>
            <li class="{{ isset($page) && $page == "lista" ? 'active' : '' }}">
                <a href="{{ url('/lista') }}"><i class="fa fa-user"></i> <span class="nav-label">Lista de Usuários</span></a>
            </li>
            <li class="{{ isset($page) && $page == "admin" ? 'active' : '' }}">
                <a href="{{ url('/admins') }}"><i class="fa fa-legal"></i> <span class="nav-label">Lista de Admins</span></a>
            </li>
            <li class="{{ isset($page) && $page == "parceiros" ? 'active' : '' }}">
                <a href="{{ url('/parceiros') }}"><i class="fa fa-money" aria-hidden="true"></i> <span class="nav-label">Parceiros</span></a>
            </li>
            <li class="{{ isset($page) && $page == "dicas" ? 'active' : '' }}">
                <a href="{{ url('/dicas') }}"><i class="fa fa-lightbulb-o"></i> <span class="nav-label">Dicas</span></a>

            </li>
            <li class="{{ isset($page) && $page == "notificacao" ? 'active' : '' }}">
                <a href="{{ url('/notificacao') }}"><i class="fa fa-bell-o" aria-hidden="true"></i> <span class="nav-label">Notificações</span></a>
            </li>
        </ul>
    </div>
</nav>