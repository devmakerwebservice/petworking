@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Cadastros
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Lista de pets cadastrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    @include('admin.cadastros.table.index')
                </div>
            </div>
        </div>
    </div>

    @include('admin.includes.modal')

@endsection


@section('scripts-footer')
    <script>
        var oTable = $('#service-table').DataTable({
            bLengthChange: false,
            bSort : true,
            bInfo : false,
            bFilter: true,
            processing: false,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [1,2,3,4,5],
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ],
            ajax: {
                "url": '{!! route('cadastros.table') !!}',
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'foto', name: 'foto', orderable: false},
                { data: 'name', name: 'name' },
                { data: 'others_info', name: 'others_info', orderable: false},
                { data: 'breed', name: 'breed' },
                { data: 'age', name: 'age' },
                { data: 'perdido', name: 'perdido' },
                { data: 'view', name: 'view', orderable: false},
                { data: 'delete', name: 'delete', orderable: false}
            ]
        });

        document.querySelector('[aria-controls="service-table"]').className += ' excel';
    </script>
@endsection
