@extends('admin.layout.dashboard')

@section('styles-head')

    <style>
        .custom-table{
            border-left: 5px solid #62D2E0;
            padding-left: 5px;
        }
    </style>

@endsection

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-6">
            <div class="widget style1 lazur-bg">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-user fa-5x"></i>
                    </div>

                    <div class="col-xs-8 text-left">
                        <h2> Usuários Cadastrados </h2>
                        <h4 class="font-bold">{{ count($users) }}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-paw fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-left">
                        <h2> Pets Cadastrados </h2>
                        <h4 class="font-bold">{{ count($animals) }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Usuários recém cadastrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    <table class="table">
                        <tbody>
                            @foreach($usersnew as $user)
                                <tr style="padding-left: 0">
                                    <td class="col-md-5" style="padding-left: 0">
                                        <span class="font-bold custom-table">
                                            {{ $user->name }}
                                        </span>
                                    </td>
                                    <td class="col-md-5">{{ $user->email }}</td>
                                    <td class="col-md-2">{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Pets recém cadastrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    <table class="table">
                        <tbody>
                            @foreach($animalsnew as $animal)
                                <tr style="padding-left: 0">
                                    <td class="col-md-5" style="padding-left: 0">
                                        <span class="font-bold custom-table">
                                             {{ $animal->name }}
                                        </span>
                                    </td>
                                    <td class="col-md-5">{{ $animal->user }}</td>
                                    <td class="col-md-2">{{ \Carbon\Carbon::parse($animal->created_at)->format('d/m/Y')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts-footer')

@endsection
