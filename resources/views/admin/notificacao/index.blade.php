@extends('admin.layout.dashboard')

@section('styles-head')
    <style>
        .input-user {
            position: relative;
            outline: 0;
            border: 0;
            border-bottom: 1px solid #D1D1D1;
            padding:0;
        }
        .input-user:focus {
            border-color: #E23773 !important;
        }
    </style>
@endsection

@section('title')
    Notificação
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Envio de notificação para usuários
                    </h4>
                </div>
                <div class="ibox-content">
                    <div class="row" style="margin-top: 20px">
                        <div class="col-lg-12 text-center">
                            <form class="form-horizontal" method="post" action="{{ url('/notify') }}" style="margin: auto">
                                {{ csrf_field() }}
                                <div class="form-group text-left">
                                    <div class="col-lg-6">
                                        <label for="title">Titulo:</label>
                                        {{--<input type="text" name="title" id="title" class="form-control input-user">--}}
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>
                                    
                                    <div class="col-lg-2">
                                        <label for="type">Para:</label>
                                        <select name="type" class="form-control" id="type" required>
                                            <option value="all">Todos</option>
                                            <option value="state">Estado</option>
                                            <option value="city">Cidade</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2" id="state" style="display: none;">
                                        <label for="estado">Estado:</label>
                                        <select class="form-control" id="estado" name="estado">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2" id="city" style="display: none;">
                                        <label for="cidade">Cidade:</label>
                                        <select class="form-control" id="cidade" name="cidade" disabled>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 text-left">
                                        <label for="description" class="">Descrição</label>
                                        <textarea class="form-control" name="description" id="description" style="min-height: 200px; max-width: 100%" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 text-right">
                                        <button type="reset" class="btn btn-danger" style="padding: 6px 35px;">Cancelar</button>
                                    </div>
                                    <div class="col-lg-6 text-left">
                                        <button type="submit" class="btn btn-primary" style="padding: 6px 35px;">
                                            Enviar
                                            <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts-footer')

    <script>
        $.getJSON('/estados_cidades.json', function (data) {
            var items = [];
            var options = '<option value=""></option>';
            $.each(data, function (key, val) {
                options += '<option value="' + val.sigla + '" id="' + val.sigla + '">' + val.sigla + '</option>';
            });
            $("#estado").html(options);

            $("#estado").change(function () {
                var options_cidades = '';
                var str = "";
                $("#estado option:selected").each(function () {
                    str += $(this).text();
                });
                $.each(data, function (key, val) {
                    if(val.sigla == str) {
                        $.each(val.cidades, function (key_city, val_city) {
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        });
                    }
                });
                $("#cidade").html(options_cidades);
            }).change();
        });
    </script>

@endsection
