@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Lista de Admins
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Lista de administradores cadastrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    @include('admin.admins.table.index')
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts-footer')
    <script>
        var oTable = $('#admin-table').DataTable({
            bLengthChange: false,
            bSort : true,
            bInfo : false,
            bFilter: true,
            processing: false,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0,1],
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ],
            ajax: {
                "url": '{!! route('admins.table') !!}',
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email', orderable: false},
                { data: 'delete', name: 'delete', orderable: false}
            ]
        });

        document.querySelector('[aria-controls="admin-table"]').className += ' excel';
    </script>
@endsection
