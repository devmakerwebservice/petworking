@extends('admin.layout.dashboard')

@section('styles-head')

@endsection

@section('title')
    Econtrados
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4 class="font-bold">
                        Lista de pets encontrados
                    </h4>
                </div>
                <div class="ibox-content" style="padding: 0">
                    @include('admin.encontrados.table.index')
                </div>
            </div>
        </div>
    </div>

    @include('admin.includes.modal')

@endsection


@section('scripts-footer')
    <script>
        var oTable = $('#encontrados-table').DataTable({
            bLengthChange: false,
            bSort : true,
            bInfo : false,
            bFilter: true,
            processing: false,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [1,2,3,4,5,6],
                        modifier: {
                            page: 'current'
                        }
                    }
                }
            ],
            ajax: {
                "url": '{!! route('encontrados.table') !!}',
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'foto', name: 'foto', orderable: false},
                { data: 'name', name: 'name' },
                { data: 'perdido', name: 'perdido'},
                { data: 'achado', name: 'achado' },
                { data: 'local', name: 'local', orderable: false},
                { data: 'breed', name: 'breed' },
                { data: 'age', name: 'age' },
                { data: 'view', name: 'view', orderable: false},
                { data: 'delete', name: 'delete', orderable: false}
            ]
        });

        document.querySelector('[aria-controls="encontrados-table"]').className += ' excel';
    </script>
@endsection
