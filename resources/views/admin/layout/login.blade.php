<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('fonts/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ url('css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('css/dashboard-style.css') }}" rel="stylesheet">
    <link href="{{ url('css/custom-style.css') }}" rel="stylesheet">
    <link href="{{ url('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
</head>

<body>

<div class="middle-box text-center loginscreen animated fadeInDown" style="width: 40%; background-color: #fff; box-shadow: 0 0 40px #a5a5a5;max-width: none;padding: 40px 0;">
    <div>
        <div>

            <img src="{{ asset('img/logo_login.png') }}" style="width: 200px" alt="">

        </div>
        <center>
        <form class="m-t" role="form" action="{{ url('/dashboard') }}" method="GET" style="width: 70%">
            <div class="form-group" id="email">
                <input type="email" name="email" class="form-control" placeholder="E-MAIL" required="">
            </div>
            <div class="form-group" id="email">
                <input type="password" name="password" class="form-control" placeholder="SENHA" required="">
            </div>
            <a href="" class="esqueci-senha pull-right" style="text-decoration: underline; color: #000; font-size: 14px; margin-bottom: 20px"><small>Esqueci minha senha</small></a>

            <button type="submit" class="btn btn-primary block full-width">Entrar</button>

        </form>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ url('js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ url('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('js/plugins/select2/select2.full.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ url('js/inspinia.js') }}"></script>
<script src="{{ url('js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ url('js/plugins/chartJs/Chart.min.js') }}"></script>
<script src="{{ url('js/plugins/chartjs-demo.js') }}"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.5/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Mar 2016 15:19:19 GMT -->
</html>
