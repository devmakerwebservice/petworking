<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Petworking | @yield('title')</title>

    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('fonts/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ url('css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('css/dashboard-style.css') }}" rel="stylesheet">
    <link href="{{ url('css/custom-style.css') }}" rel="stylesheet">

    <link href="{{ url('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/plugins/footable/footable.core.css') }}" rel="stylesheet">
    <link href="{{ url('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ url('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    @yield('styles-head')
</head>

<body class="">

<div id="wrapper">

    @include('admin.includes.menu')


    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            @include('admin.includes.header-bar')
        </div>

        <div class="wrapper wrapper-content">
            @if(Session::has('error'))
                <div class="row" style="margin-top: 15px">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ (Session::get('error')) }}
                        </div>
                    </div>
                </div>
            @endif

            @if(Session::has('success'))
                <div class="row" style="margin-top: 15px">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ (Session::get('success')) }}
                        </div>
                    </div>
                </div>
            @endif
            @yield('content')
        </div>

        {{--<div class="footer">--}}
            {{--@include('admin.includes.footer')--}}
        {{--</div>--}}

    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ url('js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('js/inspinia.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/custom.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ url('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ url('js/plugins/chartJs/Chart.min.js') }}"></script>
<script src="{{ url('js/plugins/chartjs-demo.js') }}"></script>
<script src="{{ url('js/plugins/footable/footable.all.min.js') }}"></script>
<script src="{{ url('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ url('js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Plugins -->

@yield('scripts-footer')

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.5/empty_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Mar 2016 15:21:53 GMT -->
</html>

