<?php

namespace App\Lib;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class Pusher
{
    const GOOGLE_GCM_URL = 'https://android.googleapis.com/gcm/send';

    private $apiKey;
    private $proxy;
    private $output;

    public function __construct($proxy = null)
    {
        $this->apiKey = config("app.push");
        $this->proxy  = $proxy;
    }

    /**
     * @param string|array $regIds
     * @param string $data
	 * @internal param $to
     * @throws \Exception
     */
    public function notify($data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::GOOGLE_GCM_URL);
        if (!is_null($this->proxy)) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        }
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostFields($data));

        $result = curl_exec($ch);

        if ($result === false) {
            throw new \Exception(curl_error($ch));
        }

        curl_close($ch);

        $this->output = $result;
    }

    /**
     * @return array
     */
    public function getOutputAsArray()
    {
        return json_decode($this->output, true);
    }

    /**
     * @return object
     */
    public function getOutputAsObject()
    {
        return json_decode($this->output);
    }

    private function getHeaders()
    {
        return [
            'Authorization:key=AIzaSyBQhH4TX463f5Doc0NF6k6C5xZikFHkwz8',
            'Content-Type:application/json'
        ];
    }

    private function getPostFields($data)
    {

        if(isset($data['type']))
        {
            if ($data['model'] == 'Android') {
                $fields = array(
                    'to' => $data['token'],
                    'data' => array('title' => $data['title'], 'body' => $data['content']),
                    "content_available" => true,
                    "priority" => "high"
                );
            } else {
                $fields = array(
                    'to' => $data['token'],
                    'notification' => array('title' => $data['title'], 'body' => $data['content'], "badge" => "0", "sound" => ""),
                    "content_available" => true,
                    "priority" => "high"
                );
            }
        }
        else {
            if($data['model'] === 'Android')
            {
                $fields = array(
                    'to' => $data['token'],
                    'data' => array('title' => $data['title'], 'body' => $data['content'], 'chat_id' => $data['chat_id']),
                    "content_available" => true,
                    "priority" => "high"
                );
            } else {
                $fields = array(
                    'to' => $data['token'],
                    'notification' => array('title' => $data['title'], 'body' => $data['content'], "badge" => "0", "sound" => ""),
                    'data' => array('chat_id' => $data['chat_id']),
                    "content_available" => true,
                    "priority" => "high"
                );
            }
        }

        return json_encode($fields, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }
}