<?php

namespace App\Providers;

use App\Entities\Animal;
use App\Entities\User;
use App\Lib\Pusher;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Mail;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('chat', function($data){

            $user = User::select('*')
                ->where('id', $data->user_response)
                ->first();

            $animal = Animal::select('name')
                ->where('id', $data->animal_id)
                ->first();

            $push = new Pusher();

            $push->notify([
                'token'     => $user->device_token,
                'model'     => $user->device_model,
                'title'     => $animal->name,
                'content'   => $data->message,
				'chat_id'   => $data->chat_id
            ]);
        });

        Event::listen('notification', function($data,$user){

            $push = new Pusher();

            foreach ($user as $u)
            {
                $push->notify([
                    'token'     => $u->device_token,
                    'model'     => $u->device_model,
                    'title'     => $data['title'],
                    'content'   => $data['description'],
                    'type'      => $data['push'],
                ]);
            }

        });

        Event::listen('password', function ($user,$password) {

            Mail::send('email.password', ['password' => $password, 'user' => $user], function ($message) use ($user)
            {
                $message->from('contato@petworking.com.br', 'Contato Petworking');
                $message->to($user->email);
                $message->subject("Nova Senha Petworking");
            });
        });

        Event::listen('anuncio', function ($data) {

            Mail::send('email.anuncio', ['data' => $data], function ($message) use ($data)
            {
                $message->from($data['email'], $data['name']);
                $message->to('contato@petworking.com.br');
                $message->subject("Nova requisição de parceiro.");
            });
        });
    }
}
