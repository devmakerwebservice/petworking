<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/11/2017
 * Time: 17:48
 */

namespace App\Services;


use App\Entities\Dicas;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Http\Resources\Dicas as DicasResource;

class DicasService
{
    /*
     * Retorna as dicas aprovadas
     */
    public function index()
    {
        try {
            $model = DicasResource::collection(Dicas::where('status', 1)->get());

            if($model->count() > 0){
                return response([
                    'success' => 'true',
                    'data' => $model
                ],200);
            }

            return response([
                'success' => 'false',
                'message' => 'Nenhuma dica cadastrada ainda'
            ],400);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Envia dicas
     */
    public function create($data)
    {
        try {
            $validator = $this->validator($data);

            if($validator->fails()){
                return response([
                    'success' => false,
                    'message' => $validator->errors()->first()
                ], 400);
            }

            $model = Dicas::create($data);

            return response([
                'success' => 'true',
                'message' => 'Sua dica foi enviada com sucesso!',
                'data' => new DicasResource($model)
            ],200);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ], 400);
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'description' => 'required',
            'user_id' => 'required'
        ]);
    }
}