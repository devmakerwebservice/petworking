<?php

namespace App\Services;

use App\Entities\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthService
{
    /*
     * Função para login
     */
    public function login($data)
    {
        try {
            $this->validatorUser($data);

            $user = User::where('email', $data['email'])->get();

            if($user->count() > 0) {
                $user = $user->first();
                if ((!is_null($user->password)) && Hash::check($data['password'], $user->password)){
                    Log::info(['login' => $user->email]);
                    return response([
                        'success'   => true,
                        'message'   => 'Logado',
                        'data'      => new UserResource($user)
                    ],200);
                } else {
                    Log::info(['senha incorreta' => $user->email]);
                    return response([
                        'success'   => false,
                        'message'   => 'Senha Incorreta'
                    ],401);
                }
            } else {
                return response([
                    'success' => 'false',
                    'messsage' => 'Usuário não encontrado'
                ],401);
            }
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Função para login com facebook
     */
    public function facebook($data)
    {
        try {
            $this->validatorFacebook($data);

            $user = User::where('facebook_id', $data['facebook_id'])->get();

            if($user->count() > 0) {
                $user = $user->first();

                return response([
                    'success'   => true,
                    'message'   => 'Logado',
                    'data'      => new UserResource($user)
                ],200);
            } else {
                return response([
                    'success' => 'false',
                    'messsage' => 'Usuário não encontrado'
                ],401);
            }
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }
    
    protected function validatorUser(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required'
        ]);
    }

    protected function validatorFacebook(array $data)
    {
        return Validator::make($data, [
            'facebook_id' => 'required'
        ]);
    }
}