<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 06/11/2017
 * Time: 10:34
 */

namespace App\Services;


use App\Entities\Animal;
use App\Entities\Photos;
use App\Entities\User;
use App\Http\Resources\Animal as AnimalResource;
use App\Http\Resources\AnimalColletion;
use App\Http\Resources\Photos as PhotosResource;
use App\Http\Controllers\BaseFunctionTrait;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Malhal\Geographical\Geographical;

class AnimalService
{
    use BaseFunctionTrait;

    /*
     * Função para cadastro de animal
     * STATUS = (0) Achei o pet, (1) Perdi o pet, (2) Doação
     * ESPECIE = (0) Cão, (1) Gato, (2) Outro
     * PORTE = (0) Pequeno, (1) Médio, (2) Grande, (3) Todos
     */
    public function create($data)
    {
        try {
            $validator = $this->validator($data);

            if($validator->fails()){
                return response([
                    'success' => false,
                    'message' => $validator->errors()->first()
                ], 400);
            }

            $model = Animal::create($data);

            return response([
                'success' => 'true',
                'message' => 'Pet cadastrado com sucesso!',
                'data' => new AnimalResource($model)
            ],200);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Função para atualizar o animal
     */
    public function update($data, $id)
    {
        try {
            $animal = Animal::find($id);

            if($animal){
                $animal->update($data);
                $animal->save();

                return response([
                    'success' => 'true',
                    'message' => 'Pet atualizado com sucesso!',
                    'data' => new AnimalResource($animal)
                ],200);
            }

            return response([
                'success' => 'false',
                'message' => 'Pet não encontrado'
            ],404);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);

        } catch (ModelNotFoundException $e) {

            return response([
                'success' => false,
                'message' => 'Usuário não encontrado'
            ],404);
        }
    }

    /*
     * Função para upload de imagem do animal
     */
    public function animalImage($data, $id)
    {
        try {
            $image = $this->uploadResize($data, 'animal');

            $photo['path_photo'] = $image;
            $photo['animal_id'] = $id;

            $model = Photos::create($photo);

            return response([
                'success' => 'true',
                'data' => new PhotosResource($model)
            ],200);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Função para visualizar um animal
     */
    public function find($id)
    {
        try {
            $animal = Animal::find($id);

            if($animal){
                return response([
                    'success' => true,
                    'data' => new AnimalResource($animal)
                ], 200);
            }

            return response([
                'success' => false,
                'message' => 'Animal não encontrado'
            ], 404);
        } catch (ModelNotFoundException $e) {
            return response([
                'success' => false,
                'message' => 'Usuário não encontrado'
            ], 404);
        }
    }

    /*
     * Função para exclusão de uma imagem do animal
     */
    public function deleteImages($data)
    {
        try {
            $photo = Photos::where('path_photo', $data['path_photo'])->first();

            $folder[0] = 'big';
            $folder[1] = 'original';
            $folder[2] = 'medium';
            $folder[3] = 'small';

            if($photo){
                $image = explode('/', $photo->path_photo);
                $image = end($image);

                for($i = 0; $i < count($folder); $i++){
                    $file = public_path().'/storage/animal/'.$folder[$i].'/'.$image;
                    if(file_exists($file))
                    {
                        unlink($file);
                    }
                }

                $photo->delete();

                return response([
                    'success' => 'true',
                    'message' => 'Imagem deletada com sucesso'
                ],200);
            }

            return response([
                'success' => 'false',
                'message' => 'Animal não encontrado'
            ],400);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Função para trazer o feed dos animais com o filtro
     */
    public function index($req)
    {
        $query = (new Animal())->newQuery();

        if($req->has('status')){
            $query->where('animals.status', $req->input('status'));
        } else {
            $query->where('animals.status', '<', 3);
        }

        if($req->has('species')){
            $query->where('animals.species', $req->input('species'));
        }

        if($req->has('genre')){
            $query->where('animals.genre', $req->input('genre'));
        }

        if($req->has('state')){
            $query->where('animals.state', $req->input('state'));
        }

        if($req->has('city')){
            $query->where('animals.city', $req->input('city'));
        }

        if($req->has('neighborhood')){
            $query->where('animals.neighborhood', $req->input('neighborhood'));
        }

        if($req->has('search')){
            $query->where('animals.name', 'like', $req->input('search'));
        }

        if($req->has('latitude') && $req->has('longitude')){
            $latitude = $req->get('latitude');
            $longitude = $req->get('longitude');
            $query->whereDistanceOfParent($latitude, $longitude, User::class, 'user_id');
            $query->orderBy('distance', 'asc');
        }

        $animal = new AnimalColletion($query->paginate(10));

        $pagination = $this->pagination($animal, $req->all());

        return response([
            'success' => 'true',
            'data' => $animal,
            'pagination' => $pagination
        ],200);
    }

    /*
     * Função para visualizar os meus pets
     */
    public function myPets($id)
    {
        try {
            $animal = AnimalResource::collection(Animal::where('user_id', $id)->paginate(10));

            $pagination = $this->pagination($animal);

            return response([
                'success' => 'true',
                'data' => $animal,
                'pagination' => $pagination
            ],200);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    public function pagination($data, $filters = [])
    {
        $url = '';
        $filters = collect($filters);
        $filters->pull('page');

        if($filters->isNotEmpty()) {
            $url .= '&'.$filters->map(function($filter, $key){
                return $key.'='.$filter;
            })->implode('&');
        }

        $pagination['total'] = $data->total();
        $pagination['count'] = $data->count();
        $pagination['per_page'] = $data->perPage();
        $pagination['current_page'] = $data->currentPage();
        $pagination['links']['next'] = ($nextUrl = $data->nextPageUrl()) ? $nextUrl.$url : $nextUrl;
        $pagination['links']['previous'] = ($prevUrl = $data->previousPageUrl()) ? $prevUrl.$url : $prevUrl;

        return $pagination;
    }

    public function destroy($id)
    {
        try {
            $animal = Animal::find($id);

            if($animal && $animal->status < 3){
                if($animal->status == 1) {
                    $animal->update(['status' => 3]);
                    $animal->save();

                    $message = 'Uhull! Um pet encontrado é um pet feliz';
                } else {
                    $animal->update(['status' => 4]);
                    $animal->save();

                    $message = 'Uhull! Um pet adotado é um pet feliz';
                }

                return response([
                    'success' => 'true',
                    'message' => $message
                ],200);

            }

            return response([
                'success' => 'false',
                'message' => 'Pet não encontrado!'
            ],400);

        } catch (ModelNotFoundException $e) {
            return response([
                'success' => false,
                'message' => 'Usuário não encontrado'
            ], 404);
        }
    }

    /*
     * Paramêtros de validação do animal
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'status' => 'required',
            'species' => 'required',
            'other_specie' => 'required_if:species,==,2',
            'size' => 'required',
            'genre' => 'required',
            'breed' => 'required',
            'date_lost' => 'required_if:status,==,1',
            'date_find' => 'required_if:status,==,0',
            'state' => 'required',
            'city' => 'required',
            'neighborhood' => 'required',
            'user_id' => 'required'
        ]);
    }
}