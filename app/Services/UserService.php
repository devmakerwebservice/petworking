<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 01/11/2017
 * Time: 17:21
 */

namespace App\Services;


use App\Entities\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserService
{
    /*
     * Função para criação de usuários
     */
    public function create($data)
    {
        try {
            $validator = $this->validator($data);

            if($validator->fails()){
                return response([
                    'success' => false,
                    'message' => $validator->errors()->first()
                ], 400);
            }

            $model = User::create($data);

//            event('welcome.user', $model);

            return response([
                'success' => true,
                'message' => 'Seu cadastro foi realizado com sucesso',
                'data' => new UserResource($model)
            ],200);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Função para criação de usuário com o facebook
     */
    public function createFacebook($data)
    {
        try {
            $validator = $this->validator($data);

            if($validator->fails()){
                return response([
                    'success' => false,
                    'message' => $validator->errors()->first()
                ], 400);
            }

            $data['path_foto'] = "https://graph.facebook.com/".$data['facebook_id']."/picture?type=large";
            $data['role'] = 'user';

            $model = User::create($data);

//            event('welcome.user', $model);

            return response([
                'success' => true,
                'message' => 'Seu cadastro foi realizado com sucesso',
                'data' => new UserResource($model)
            ],200);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);
        }
    }

    /*
     * Função para atualizar o cadastro do usuário
     */
    public function update($data, $id)
    {
        try {
            $user = User::find($id);

            if($user){
                if(isset($data['new_password']))
                {
                    if (Hash::check($data['password'], $user->password)){
                        if($data['new_password'] === $data['confirm_password']){
                            $data['password'] = $data['new_password'];
                        }
                        else{
                            return response([
                               'success' => false,
                               'message' => 'As senhas não correspondem.'
                            ],400);
                        }
                    }
                    else{
                        return response([
                            'success' => false,
                            'message' => 'Senha atual não corresponde.'
                        ],400);
                    }
                }
                $user->update($data);
                $user->save();

                return response([
                    'success' => 'true',
                    'message' => 'Seu cadastro foi atualizado com sucesso',
                    'data' => new UserResource($user)
                ],200);
            }

            return response([
                'success' => 'false',
                'message' => 'Usuário não encontrado'
            ],404);
        } catch (ValidationException $e) {
            $err = $e->validator->getMessageBag()->all();
            $err = array_pop($err);

            return response([
                'success' => false,
                'message' => $err
            ],400);

        } catch (ModelNotFoundException $e){

            return response([
                'success' => false,
                'message' => 'Usuário não encontrado'
            ],404);
        }
    }

    /*
     * Paramêtros de validação
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'facebook_id' => 'required_without:password|unique:users,facebook_id'
        ]);
    }
}