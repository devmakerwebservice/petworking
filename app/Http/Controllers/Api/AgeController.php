<?php

namespace App\Http\Controllers\Api;

use App\Entities\Ages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgeController extends Controller
{
    public function index()
    {
        return response([
            'success' => 'true',
            'data' => Ages::all()
        ],200);
    }
}
