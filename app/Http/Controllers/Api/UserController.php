<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseFunctionTrait;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    use BaseFunctionTrait;

    /**
     * @var UserService
     */
    private $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

		if(!isset($data['facebook_id']) || $data['facebook_id'] === null || $data['facebook_id'] === "") {
			$data['role'] = 'user';
			
			if(isset($data['path_foto'])){
				$data['path_foto'] = $this->uploadResize($data['path_foto'], 'user');
			} else {
				$data['path_foto'] = asset('img/avatar.png');
			}
			unset($data['facebook_id']);
			
			return $this->service->create($data);
		} else {
            return $this->service->createFacebook($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if(isset($data['path_foto'])){
            $data['path_foto'] = $this->uploadResize($data['path_foto'], 'user');
        }

        return $this->service->update($data, $id);
    }
}
