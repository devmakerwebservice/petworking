<?php

namespace App\Http\Controllers\Api;

use App\Services\DicasService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DicasController extends Controller
{
    /**
     * @var DicasService
     */
    private $service;

    /**
     * DicasController constructor.
     * @param DicasService $service
     */
    public function __construct(DicasService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }
    
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }
}
