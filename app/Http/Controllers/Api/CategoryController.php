<?php

namespace App\Http\Controllers\Api;

use App\Entities\Category;
use App\Entities\Partners;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partners as PartnersResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return response([
            'success' => true,
            'data' => Category::all()
        ],200);
    }

    public function find(Request $request, $id)
    {
        $query = (new Partners())->newQuery();

        $query->where('category_id', $id);

        if($request->has('latitude') && $request->has('longitude')) {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');
            $query->whereDistanceOfParent($latitude, $longitude);
            $query->orderBy('distance', 'asc');
        }

        $partners = PartnersResource::collection($query->get());

        return response([
            'success' => true,
            'data' => $partners
        ],200);
    }

    public function banner ()
    {
        $banner = Partners::select('path_banner')
            ->whereRaw('path_banner is not null')
            ->get();

        if(count($banner) > 0) {
            return response([
               'success' => true,
                'data' => $banner
            ],200);
        } else {

        $banner['path_banner'] =  asset('img/petworking_banner.png');

            return response([
                'success' => true,
                'data' => array($banner)
            ],200);
        }
    }

    public function request (Request $request)
    {
        $data = $request->all();

        event('anuncio', array($data));

        return response([
            'success' => true,
            'message' => 'E-mail encaminhado com sucesso. Responderemos assim que possível.'
        ],200);
    }
}
