<?php

namespace App\Http\Controllers\Api;

use App\Entities\User;
use App\Services\AuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @var AuthService
     */
    private $service;

    /**
     * AuthController constructor.
     * @param AuthService $service
     */
    public function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    public function login(Request $request)
    {
        $data = $request->all();

        if(isset($data['facebook_id'])){
            return $this->service->facebook($request->all());
        }

        return $this->service->login($data);
    }

    public function password(Request $request)
    {
        $data = $request->all();

        $user = User::select()
            ->where('users.email', $data['email'])
            ->first();

        if ($user)
        {
            $password = rand(1000,9999);
            $user->password = $password;
            $user->save();

            event('password',[$user,$password]);

            return response([
                'success' => true,
                'message' => 'Nova senha enviada por email.'
            ],200);
        }
        else{
            return response([
                'success' => false,
                'message' => 'Usuário não encontrado.'
            ],400);
        }

    }
}
