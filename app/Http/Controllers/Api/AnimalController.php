<?php

namespace App\Http\Controllers\Api;

use App\Services\AnimalService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnimalController extends Controller
{
    /**
     * @var AnimalService
     */
    private $service;

    /**
     * AnimalController constructor.
     * @param AnimalService $service
     */
    public function __construct(AnimalService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->service->index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * Controller para salvar foto do animal
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function images(Request $request, $id)
    {
        $data = $request->all();

        return $this->service->animalImage($data['path_photo'], $id);
    }

    /**
     * Função para deletar imagem
     *
     * @param Request $request
     * @return mixed
     */
    public function deleteImages(Request $request)
    {
        return $this->service->deleteImages($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

    public function myPets($id)
    {
        return $this->service->myPets($id);
    }
}
