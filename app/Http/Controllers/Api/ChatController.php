<?php

namespace App\Http\Controllers\Api;

use App\Entities\Chat;
use App\Entities\ChatUsers;
use App\Entities\Posts;
use App\Entities\User;
use App\Entities\Animal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {		
		$chats = DB::select("
			SELECT 
				cu.*, c.animal_id, u.name, u.id AS user_id
			FROM
			chat_users cu
				INNER JOIN
			users u ON u.id = cu.user_id
				INNER JOIN
			chats c ON c.id = cu.chat_id
			WHERE cu.user_id <> $id
				AND cu.chat_id IN (SELECT chat_id FROM chat_users WHERE user_id = $id)");
			
		if(count($chats) > 0){
			return response([
				'success' => true,
				'chats' => $chats
			], 200);
		} else {
			return response([
				'success' => false,
				'message' => 'Você não possui chats ainda'
			], 200);
		}
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $users = ['user_sender' => $data['user_sender'], 'user_response' => $data['user_response']];

        if(!isset($data['chat_id']) && empty($data['chat_id']))
        {
            $chat = new Chat;

            $chat->animal_id = $data['animal_id'];

            $ent = $chat->save();

            if($ent) {
                $data['chat_id'] = $chat->id;

                foreach($users as $user){

                    $chat_users = new ChatUsers;

                    $chat_users->chat_id = $chat->id;
                    $chat_users->user_id = $user;

                    $chat_users->save();
                }
            }
        }

        $message = new Posts;

        $message->chat_id = $data['chat_id'];
        $message->user_id = $data['user_sender'];
        $message->message = $data['message'];

        $message->save();

        $message->user_response = $data['user_response'];
        $message->animal_id = $data['animal_id'];

        event('chat',$message);

        return response([
          'success' => true,
          'message' => $message,
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = ChatUsers::select('users.name','users.id')
			->join('users', 'chat_users.user_id', '=', 'users.id')
            ->where('chat_users.chat_id', $id)
            ->get();

        $messages = Posts::select('posts.id', 'posts.user_id', 'posts.message')
            ->where('posts.chat_id', $id)
            ->get();
			
		$chat = Chat::find($id);
		
		if(count($chat) > 0) {
			$animal = Animal::find($chat->animal_id);
		} else {
			$animal = '';
		}

        return response([
            'success' => true,
            'messages' => $messages,
            'users' => $users,
			'animal' => $animal
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chat = Chat::find($id);

        $chat->delete();

        return response([
            'success' => true,
            'message' => 'Chat deletado com sucesso.',
        ], 200);
    }
}
