<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Entities\Partners;
use App\Http\Controllers\BaseFunctionTrait;
use Geocoder\Laravel\Facades\Geocoder;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jcf\Geocode\Geocode;
use Yajra\DataTables\DataTables;

class PartnersController extends Controller
{
    use BaseFunctionTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'parceiros';
        return view('admin.parceiros.index')->with(compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'parceiros';
        $categories = Category::all();
        return view('admin.parceiros.new')->with(compact('page', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data,[
            'name' => 'required',
            'phone' => 'required',
            'cep' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'number' => 'required',
            'category_id' => 'required',
        ],[
            'name.required' => 'Nome obrigatório.',
            'phone.required' => 'Telefone obrigatório.',
            'cep.required' => 'CEP obrigatório.',
            'address.required' => 'Endereço obrigatório.',
            'city.required' => 'Cidade obrigatório.',
            'state.required' => 'Estado obrigatório.',
            'number.required' => 'Número obrigatório.',
            'category_id.required' => 'Categoria obrigatória.',

        ]);

        if ($validator->fails()){
            return back()->with('error', $validator->errors()->first());
        } else {
            if($data['banner'] == 't'){
                $file = $data['path_banner'];
                $name = $file->getPathName();
                $file = base64_encode(file_get_contents($name));

                $data['path_banner'] = $this->uploadResize($file, 'banner');
            }

            $model = Partners::create($data);

            try{
                $model->updLatLong();
            } catch (\Exception $e) {
                return back()->with('error', $e->getMessage());
            }

            return back()->with('success', 'Parceiro criado com sucesso.');
        }
    }

    public function getAddress($data)
    {
        return $data['address'].', '.$data['number'].', '.$data['city'].', '.$data['state'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $partner = Partners::select('partners.*', 'categories.name as category')
            ->join('categories', 'partners.category_id', '=', 'categories.id')
            ->where('partners.id', $id)
            ->first();

        if ($partner->path_banner == null){
            $partner->path_banner = asset('img/no_image.png');
        }

        return [
            'success' => true,
            'ptn' => $partner
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Partners::find($id);

        $categories = Category::all();

        return view('admin.parceiros.edit')->with(compact('page','data', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $parceiro = Partners::find($data['id']);

        $folder[0] = 'big';
        $folder[1] = 'original';
        $folder[2] = 'medium';
        $folder[3] = 'small';

        $image = explode('/', $parceiro->path_banner);
        $image = end($image);

        if($data['banner'] === 'f'){
            for($i = 0; $i < count($folder); $i++){
                $file = public_path().'/storage/banner/'.$folder[$i].'/'.$image;
                if(file_exists($file)) {
                    unlink($file);
                }
            }
            $parceiro->path_banner = null;
        } else if (isset($data['path_banner']) && $data['banner'] === 't') {
            for($i = 0; $i < count($folder); $i++){
                $file = public_path().'/storage/banner/'.$folder[$i].'/'.$image;
                if($parceiro->path_banner != null) {
                    unlink($file);
                }
            }
            $foto = $data['path_banner'];
            $name = $foto->getPathName();
            $foto = base64_encode(file_get_contents($name));
            $data['path_banner'] = $this->uploadResize($foto, 'banner');
            $parceiro->path_banner = $data['path_banner'];
        }

        $parceiro->name = $data['name'];
        $parceiro->address = $data['address'];
        $parceiro->state = $data['state'];
        $parceiro->city = $data['city'];
        $parceiro->number = $data['number'];
        $parceiro->cep = $data['cep'];
        $parceiro->comp = $data['comp'];
        $parceiro->phone = $data['phone'];
        $parceiro->category_id = $data['category_id'];

        $parceiro->save();

        return back()->with('success', 'Dados alterados com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parceiro = Partners::find($id);

        if(isset($parceiro->path_banner)){
            $folder[0] = 'big';
            $folder[1] = 'original';
            $folder[2] = 'medium';
            $folder[3] = 'small';

            $image = explode('/', $parceiro->path_banner);
            $image = end($image);

            for($i = 0; $i < count($folder); $i++){
                $file = public_path().'/storage/banner/'.$folder[$i].'/'.$image;
                if(file_exists($file)) {
                    unlink($file);
                }
            }
        }

        $parceiro->delete();

        return ['success' => true];
    }

    /**
     * Generate table of partners.
     *
     * @return \Illuminate\Http\Response
     */
    public function table()
    {

        $model = Partners::select('partners.*', 'categories.name as category')
            ->join('categories', 'partners.category_id', '=', 'categories.id')
            ->get();

        return DataTables::of($model)
            ->addColumn('banner', function($model){
                return $model->path_banner != null ? '<img width="150" height="80" src="'.$model->path_banner.'">' : '-';
            })
            ->addColumn('delete', function($model) {
                return '<a onclick="delete_partner(this.dataset.partner_id)" data-partner_id="' . $model->id . '" style="color: red;font-size: 18px"><i class="fa fa-close"></i></button>';
            })
            ->addColumn('view', function($model) {
                return '<a onclick="view_partner(this.dataset.partner_id)" data-partner_id="' . $model->id . '" style="font-size: 18px"><i class="fa fa-eye"></i></button>';
            })
            ->addColumn('editar', function($model) {
                return '<a href="'. url("/parceiro/edit/".$model->id) .'"  data-tip_id="' . $model->id . '" style="font-size: 18px"><i class="fa fa-pencil"></i></button>';
            })
            ->addColumn('local', function($model) {
                return $model->address.' '.$model->number.'/'.$model->city.'-'.$model->state;
            })
            ->rawColumns(['banner', 'delete', 'view', 'editar', 'local'])
            ->make(true);

    }
}
