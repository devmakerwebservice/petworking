<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Entities\Dicas;
use App\Entities\Partners;
use App\Http\Controllers\BaseFunctionTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Animal;
use App\Entities\User;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    use BaseFunctionTrait;

    public function index()
    {
        $page = 'dashboard';
        $usersnew = User::select('*')
            ->where('role', '=', 'user')
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();
        $users = User::select('*')
            ->where('role', '=', 'user')
            ->get();
        $animals = Animal::all();
        $animalsnew = Animal::select('animals.*', 'users.name as user')
            ->join('users', 'animals.user_id', '=', 'users.id')
            ->take(10)
            ->get();

        return view('admin.dashboard.index')->with(compact('page','users','animals','usersnew','animalsnew'));
    }

    public function tips()
    {
        $page = 'dicas';
        return view('admin.dicas.index')->with(compact('page'));
    }

    public function tableTip()
    {

        $model = Dicas::all();

//        dd($model);

        return DataTables::of($model)
            ->addColumn('banner', function($model){
                return '<img class="img-circle table-foto" src="'.$model->path_banner.'">';
            })
            ->addColumn('local', function($model){
                return $model->city . '.' . $model->state;
            })
            ->addColumn('delete', function($model) {
                return '<a onclick="delete_tip(this.dataset.tip_id)" data-tip_id="' . $model->id . '" style="color: red;font-size: 18px"><i class="fa fa-close"></i></button>';
            })
            ->addColumn('editar', function($model) {
                return '<a href="'. url("/dica/edit/".$model->id) .'"  data-tip_id="' . $model->id . '" style="font-size: 18px"><i class="fa fa-pencil"></i></button>';
            })
            ->addColumn('aceitar', function($model) {
                if($model->status === 0){
                    return '<a onclick="check_tip(this.dataset.tip_id)" data-tip_id="' . $model->id . '" style="font-size: 18px"><i class="fa fa-check"></i></button>';
                } else {
                    return 'Aprovado';
                }
            })
            ->rawColumns(['banner', 'delete', 'editar', 'aceitar'])
            ->make(true);

    }

    public function destroyTip($id)
    {
        $dica = Dicas::find($id);

        $dica->delete();

        return ['success' => true];
    }

    public function checkTip($id)
    {
        $dica = Dicas::find($id);

        $dica->status = 1;

        $dica->save();

        return ['success' => true];
    }

    public function formTip($id)
    {
        $dica = Dicas::find($id);

        $page = 'dicas';
        return view('admin.dicas.edit')->with(compact('page','dica'));

    }

    public function editTip(Request $request)
    {
        $data = $request->all();

        $dica = Dicas::find($data['id']);

        if($data['description'] != $dica->description){
            $dica->description = $data['description'];

            $dica->save();

            return back()->with('success', 'Dica editada com sucesso.');
        }

        return back()->with('error', 'Nenhum dado alterado.');

    }

    public function notification()
    {
        $page = 'notificacao';
        return view('admin.notificacao.index')->with(compact('page'));
    }

    public function notify(Request $request)
    {
        $data = $request->all();

        $data['push'] = 'notification';

        if ($data['type'] === 'all')
        {
            $query = "device_token is not null";
        }

        if ($data['type'] === 'state')
        {
            $estado = $data['estado'];
            $query = "estado = '$estado' and device_token is not null";
        }

        if ($data['type'] === 'city')
        {
            $cidade = $data['cidade'];
            $query = "cidade = '$cidade' and device_token is not null";
        }

        $user = User::select()
            ->whereRaw($query)
            ->where('role', 'user')
            ->get();

        if (count($user) > 0)
        {
            event('notification',[$data,$user]);
            return back()->with('success', 'Notificação enviada com sucesso.');

        } else
        {
            return back()->with('error', 'Não existem usuários nesta região.');
        }


    }
}
