<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Animal;
use App\Entities\Photos;
use App\Entities\User;
use App\Http\Controllers\BaseFunctionTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    use BaseFunctionTrait;

    public function indexList()
    {
        $page = 'lista';
        return view('admin.lista.index')->with(compact('page'));
    }

    public function tableList()
    {
        $model = User::select('users.*', 'animals.name as pet')
            ->leftJoin('animals', 'users.id', '=', 'animals.user_id')
            ->where('role', '=', 'user')
            ->get();

        return DataTables::of($model)
            ->addColumn('pet', function($model) {
                return $model->pet == null ? '-' : $model->pet;
            })
            ->addColumn('delete', function($model) {
                return '<a onclick="delete_user(this.dataset.user_id)" data-user_id="' . $model->id . '" style="color: red;font-size: 18px"><i class="fa fa-close"></i></button>';
            })
            ->rawColumns(['delete'])
            ->make(true);
    }

    public function indexAdm()
    {
        $page = 'admin';
        return view('admin.admins.index')->with(compact('page'));
    }

    public function tableAdm()
    {
        $model = User::select('users.*')
            ->where('role', '=', 'admin')
            ->where('id', '!=', auth()->user()->id)
            ->get();

        return DataTables::of($model)
            ->addColumn('delete', function($model) {
                return '<a onclick="delete_user(this.dataset.admin_id)" data-admin_id="' . $model->id . '" style="color: red;font-size: 18px"><i class="fa fa-close"></i></button>';
            })
            ->rawColumns(['delete'])
            ->make(true);
    }

    public function user()
    {
        $data = auth()->user();

        $page = 'editar';
        return view('admin.user.edit')->with(compact('page','data'));
    }

    public function edit(Request $request)
    {
        $data = $request->all();

        $user = auth()->user();

        if(Hash::check($data['atual'], $user->password)){
            $validator = Validator::make($data,[
                'nome' => 'required',
                'email' => 'required',
                'atual' => 'required',
                'senha' => 'confirmed',
            ]);

            if ($validator->fails()){
                return back()->with('error', $validator->errors()->first());
            } else {
                $user = User::find($user->id);

                if($data['nome'] != $user->name){
                    $user->name = $data['nome'];
                }
                if($data['email'] != $user->email){
                    $user->email = $data['email'];
                }
                if(isset($data['foto'])){
                    if(!$user->path_foto === asset('avatar.png')) {
                        $folder[0] = 'big';
                        $folder[1] = 'original';
                        $folder[2] = 'medium';
                        $folder[3] = 'small';

                        $image = explode('/', $user->path_foto);
                        $image = end($image);

                        for($i = 0; $i < count($folder); $i++){
                            $file = public_path().'/storage/user/'.$folder[$i].'/'.$image;
                            if(file_exists($file)) {
                                unlink($file);
                            }
                        }
                    }

                    $file = $data['foto'];
                    $name = $file->getPathName();
                    $file = base64_encode(file_get_contents($name));

                    $data['foto'] = $this->uploadResize($file, 'user');

                    $user->path_foto = $data['foto'];
                }
                if(isset($data['senha'])){
                    $user->password = $data['senha'];
                }

                $user->save();

                return back()->with('success', 'Usuário editado com sucesso.');
            }
        }
        else{
            return back()->with('error', 'A senha atual não confere.');
        }
    }


    public function admin()
    {
        $page = 'admin';
        return view('admin.user.admin')->with(compact('page'));
    }

    public function adminNew(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data,[
            'path_foto' => 'required',
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed|min:3',
        ],[
            'path_foto.required' => 'Foto obrigatória.',
            'name.required' => 'Nome obrigatório.',
            'email.required' => 'Email obrigatório.',
            'email.unique' => 'Já existe um usuário com este email.',
            'password.required' => 'Senha obrigatória.',
            'password.min' => 'Senha com no mínimo 3 caracteres.',
            'password.confirmed' => 'As senhas não conferem.',
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        } else {

            $file = $data['path_foto'];
            $name = $file->getPathName();
            $file = base64_encode(file_get_contents($name));

            $data['path_foto'] = $this->uploadResize($file, 'user');
            $data['role'] = 'admin';

            User::create($data);

            return back()->with('success', 'Cadastrado com sucesso.');
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);

        $folder[0] = 'big';
        $folder[1] = 'original';
        $folder[2] = 'medium';
        $folder[3] = 'small';

        $image = explode('/', $user->path_foto);
        $image = end($image);

        for($i = 0; $i < count($folder); $i++){
            $file = public_path().'/storage/user/'.$folder[$i].'/'.$image;
            if(file_exists($file)) {
                unlink($file);
            }
        }


        $animals = Animal::select('*')
            ->where('user_id', '=', $id)
            ->get();

        if(isset($animals)){

            foreach ($animals as $animal){

                $photos = Photos::select()
                    ->where('animal_id', '=', $animal->id)
                    ->get();

                foreach ($photos as $photo){
                    $foto = explode('/', $photo->path_foto);
                    $foto = end($foto);

                    for($i = 0; $i < count($folder); $i++){
                        $file = public_path().'/storage/animal/'.$folder[$i].'/'.$foto;
                        if(file_exists($file)) {
                            unlink($file);
                        }
                    }

                }

            }

        }

        $user->delete();

        return ['success' => true];
    }
}
