<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Animal;
use App\Entities\Photos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\User;
use Yajra\DataTables\DataTables;

class PetsController extends Controller
{
    public function indexNew()
    {
        $page = 'cadastros';

        return view('admin.cadastros.index')->with(compact('page'));
    }

    public function tableNew()
    {
        $model = Animal::select('animals.*', 'photos.path_photo as foto')
            ->leftJoin('photos', 'animals.id', '=', 'photos.animal_id')
            ->get();

        return DataTables::of($model)
            ->addColumn('foto', function($model){
                $model->foto == null ? $model->foto = asset('img/avatar.png') : $model->foto;
                return '<img class="img-circle table-foto" src="'.$model->foto.'">';
            })
            ->addColumn('perdido', function($model){
                return $model->date_lost == null ? '-' : Carbon::parse($model->date_lost)->format('d/m/Y');
            })
            ->addColumn('view', function($model) {
                return '<a onclick="view_pet(this.dataset.pet_id)" data-pet_id="' . $model->id . '" style="font-size: 18px"><i class="fa fa-eye"></i></button>';
            })
            ->addColumn('delete', function($model) {
                return '<a onclick="delete_pet(this.dataset.pet_id)" data-pet_id="' . $model->id . '" style="color: red;font-size: 18px"><i class="fa fa-close"></i></button>';
            })
            ->rawColumns(['foto', 'perdido', 'delete', 'view'])
            ->make(true);
    }


    public function indexFind()
    {
        $page = 'encontrados';
        return view('admin.encontrados.index')->with(compact('page'));
    }

    public function tableFind()
    {
        $model = Animal::select('animals.*', 'photos.path_photo as foto')
            ->leftJoin('photos', 'animals.id', '=', 'photos.animal_id')
            ->where('animals.date_find', '<>', 'null')
            ->get();

        return DataTables::of($model)
            ->addColumn('foto', function($model){
                $model->foto == null ? $model->foto = asset('img/avatar.png') : $model->foto;
                return '<img class="img-circle table-foto" src="'.$model->foto.'">';
            })
            ->addColumn('perdido', function($model){
                return $model->date_lost == null ? '-' : Carbon::parse($model->date_lost)->format('d/m/Y');
            })
            ->addColumn('achado', function($model){
                return $model->date_find == null ? '-' : Carbon::parse($model->date_find)->format('d/m/Y');
            })
            ->addColumn('local', function($model) {
                return $model->city . '/' . $model->state;
            })
            ->addColumn('view', function($model) {
                return '<a onclick="view_pet(this.dataset.pet_id)" data-pet_id="' . $model->id . '" style="font-size: 18px"><i class="fa fa-eye"></i></button>';
            })
            ->addColumn('delete', function($model) {
                return '<a onclick="delete_pet(this.dataset.pet_id)" data-pet_id="' . $model->id . '" style="color: red;font-size: 18px"><i class="fa fa-close"></i></button>';
            })
            ->rawColumns(['foto', 'perdido', 'achado', 'delete', 'view'])
            ->make(true);
    }

    public function show($id)
    {

        $pet = Animal::select('animals.*', 'photos.path_photo as foto', 'users.name as dono')
            ->where('animals.id', $id)
            ->leftJoin('photos', 'animals.id', '=', 'photos.animal_id')
            ->join('users', 'animals.user_id', '=', 'users.id')
            ->first();

        $pet->foto == null ? $pet->foto = asset('img/avatar.png') : $pet->foto;

        return [
            'success' => true,
            'pet' => $pet
        ];
    }

    public function destroy($id)
    {

        $folder[0] = 'big';
        $folder[1] = 'original';
        $folder[2] = 'medium';
        $folder[3] = 'small';

        $pet = Animal::find($id);

        $fotos = Photos::select()
            ->where('animal_id', '=', $pet->id)
            ->get();

        for($x = 0; $x < count($fotos); $x++){
            $pet[$x]->delete();

            $image = explode('/', $fotos[$x]->path_photo);
            $image = end($image);

            for($i = 0; $i < count($folder); $i++){
                $file = public_path().'/storage/user/'.$folder[$i].'/'.$image;
                if(file_exists($file)) {
                    unlink($file);
                }
            }
        }

        $pet->delete();

        return ['success' => true];
    }
}
