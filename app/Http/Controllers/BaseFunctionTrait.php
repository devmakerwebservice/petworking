<?php

namespace App\Http\Controllers;

use Eventviva\ImageResize;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Ramsey\Uuid\Uuid;

trait BaseFunctionTrait
{
    public function uploadResize($base, $dir)
    {
        $name = Uuid::uuid4() . '.jpg';

        $image = ImageResize::createFromString(base64_decode($base));
        is_dir("app/public/$dir/original/") ? null : Storage::makeDirectory("public/$dir/original/");
        $image->save(storage_path("app/public/$dir/original/") . $name);

        $image->resizeToWidth(150);
        is_dir("app/public/$dir/small/") ? null : Storage::makeDirectory("public/$dir/small/");
        $image->save(storage_path("app/public/$dir/small/") . $name);

        $image->scale(50);
        is_dir("app/public/$dir/medium/") ? null : Storage::makeDirectory("public/$dir/medium/");
        $image->save(storage_path("app/public/$dir/medium/") . $name);

        $image->scale(80);
        is_dir("app/public/$dir/big/") ? null : Storage::makeDirectory("public/$dir/big/");
        $image->save(storage_path("app/public/$dir/big/") . $name);

        return URL::to(Storage::url("$dir/original/$name"));
    }
}