<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Partners extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'address' => $this->address,
            'number' => $this->number,
            'city' => $this->city,
            'state' => $this->state,
            'category' => $this->category,
            'path_banner' => $this->path_banner
        ];
    }
}
