<?php

namespace App\Http\Resources;

use App\Entities\Ages;
use App\Entities\User;
use Illuminate\Http\Resources\Json\Resource;

class Animal extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user;
        $data = [
            'id' => $this->id,
            'name' => $this->name != null ? $this->name : 'Pet',
            'status' => $this->status,
            'species' => $this->species,
			'age' => $this->age,
            'other_specie' => $this->other_specie,
            'size' => $this->size($this->size),
            'genre' => $this->genre == 'M' ? 'Macho' : 'Fêmea',
			'color' => $this->color,
            'breed' => $this->breed,
            'date_lost' => $this->date_lost,
            'date_find' => $this->date_find,
            'others_info' => $this->others_info,
            'state' => $this->state,
            'city' => $this->city,
            'neighborhood' => $this->neighborhood,
            'user_id' => [
                "id" => $user->id,
                "name" => $user->name,
                "email" => $user->email,
                "facebook_id" => $user->facebook_id,
                "phone" => $user->phone,
                "path_foto" => $user->path_foto,
                "status" => $user->status,
                "cidade" => $user->cidade,
                "estado" => $user->estado,
                "role" => $user->role,
                "device_token" => $user->device_token,
                "device_model" => $user->device_model,
                "latitude" => $user->latitude,
                "longitude" => $user->longitude,
            ],
            'images' => $this->images->toArray()
        ];

        if($user->distance || $user->distance == 0){
            $data['distancia'] = $user->distance;
        }

        return $data;
    }

    protected function size($size)
    {
        switch ($size)
        {
            case(0):
                return 'Pequeno';
                break;
            case(1):
                return 'Médio';
                break;
            case(2):
                return 'Grande';
                break;
            default:
                return 'Pequeno';
                break;
        }
    }
}
