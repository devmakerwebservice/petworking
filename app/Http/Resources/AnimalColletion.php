<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AnimalColletion extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->collection->map(function(\App\Entities\Animal $animal){
            $user = $animal->user;
            $data = [
                'id' => $animal->id,
                'name' => $animal->name != null ? $animal->name : 'Pet',
                'status' => $animal->status,
                'species' => $animal->species,
                'age' => $animal->age,
                'other_specie' => $animal->other_specie,
                'size' => $this->size($animal->size),
                'genre' => $animal->genre == 'M' ? 'Macho' : 'Fêmea',
                'color' => $animal->color,
                'breed' => $animal->breed,
                'date_lost' => $animal->date_lost,
                'date_find' => $animal->date_find,
                'others_info' => $animal->others_info,
                'state' => $animal->state,
                'city' => $animal->city,
                'neighborhood' => $animal->neighborhood,
                'user_id' => [
                    "id" => $user->id,
                    "name" => $user->name,
                    "email" => $user->email,
                    "facebook_id" => $user->facebook_id,
                    "phone" => $user->phone,
                    "path_foto" => $user->path_foto,
                    "status" => $user->status,
                    "cidade" => $user->cidade,
                    "estado" => $user->estado,
                    "role" => $user->role,
                    "device_token" => $user->device_token,
                    "device_model" => $user->device_model,
                    "latitude" => $user->latitude,
                    "longitude" => $user->longitude,
                ],
                'images' => $animal->images->toArray()
            ];

            return $data;
        });


        return $data->toArray();
    }

    protected function size($size)
    {
        switch ($size)
        {
            case(0):
                return 'Pequeno';
                break;
            case(1):
                return 'Médio';
                break;
            case(2):
                return 'Grande';
                break;
            default:
                return 'Pequeno';
                break;
        }
    }
}
