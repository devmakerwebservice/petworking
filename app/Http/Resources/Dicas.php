<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Dicas extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'description' => $this->description
//            'user_id' => $this->user_id
        ];
    }
}
