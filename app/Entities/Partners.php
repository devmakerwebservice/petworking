<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Jcf\Geocode\Geocode;

class Partners extends Model
{
    protected $fillable = [
        'name',
        'address',
        'city',
        'state',
        'number',
        'cep',
        'phone',
        'category_id',
        'path_banner',
        'latitude',
        'longitude'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function scopeWhereDistanceOfParent(Builder $query, $lat, $long)
    {
        $str = '((ACOS(SIN(? * PI() / 180) * SIN('.$this->getTable().'.latitude * PI() / 180) + COS(? * PI() / 180) * COS('.$this->getTable().'.latitude * PI() / 180) * COS((? - '.$this->getTable().'.longitude) * PI() / 180)) * 180 / PI()) * 60 * ?)';
        $query->selectRaw($this->getTable().".*, $str as distance", [$lat, $lat, $long, "1,853159616"]);
        $query->whereRaw($str.' BETWEEN ? and ?',[$lat, $lat, $long, "1,853159616", -0, 50]);
    }

    public function updLatLong()
    {
        $response = Geocode::make()->address($this->address.', '.$this->number.', '.$this->city.', '.$this->state);
        if (!$response) {
            throw new \Exception('Não foi possivel achar o endereco', Response::HTTP_NOT_FOUND);
        }
        $data = [
            'latitude' => $response->latitude(),
            'longitude' => $response->longitude()
        ];
        if(!$this->update($data)){
            throw new \Exception('Não foi possivel atualizar o endereco', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return true;
    }
}
