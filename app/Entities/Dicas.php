<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Dicas extends Model
{
    protected $fillable = [
        'name',
        'email',
        'description',
        'status'
//        'user_id'
    ];
}
