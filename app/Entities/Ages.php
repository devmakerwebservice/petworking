<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Ages extends Model
{
    protected $fillable = [
        'age'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
