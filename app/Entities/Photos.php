<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    protected $fillable = [
        'path_photo',
        'animal_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
