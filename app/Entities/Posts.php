<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = [
        'message',
        'chat_id',
        'user_id'
    ];
}
