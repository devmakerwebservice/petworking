<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Malhal\Geographical\Geographical;

class Animal extends Model
{
    protected $fillable = [
        'name',
        'status',
        'species',
		'age',
        'other_species',
        'size',
        'genre',
        'color',
        'breed',
        'date_lost',
        'date_find',
        'others_info',
        'state',
        'city',
        'neighborhood',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function images()
    {
        return $this->hasMany(Photos::class, 'animal_id');
    }

    public function scopeWhereDistanceOfParent(Builder $query, $lat, $long, $class, $relatedKey)
    {
        if(is_string($class)){
            $class = new $class();
        }
        /**
         * @var Entity $class
         */
        $table = $class->getTable();
        $key = $class->getKeyName();

        $str = '((ACOS(SIN(? * PI() / 180) * SIN('.$table.'.latitude * PI() / 180) + COS(? * PI() / 180) * COS('.$table.'.latitude * PI() / 180) * COS((? - '.$table.'.longitude) * PI() / 180)) * 180 / PI()) * 60 * ?)';
        $query->selectRaw($this->getTable().".*, $str as distance", [$lat, $lat, $long, "1,853159616"]);
        $query->join($table, $this->getTable().'.'.$relatedKey, '=', $table.'.'.$key);
        $query->whereRaw($str.' BETWEEN ? and ?',[$lat, $lat, $long, "1,853159616", -0, 50]);
    }
}
