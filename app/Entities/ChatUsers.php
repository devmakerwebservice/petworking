<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ChatUsers extends Model
{
    protected $fillable = [
        'chat_id',
        'user_id'
    ];
}
