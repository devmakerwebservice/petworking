<?php

namespace App\Console\Commands;

use App\Entities\Partners;
use Illuminate\Console\Command;
use Jcf\Geocode\Geocode;

class UpdateLatLong extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:partners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pega os partners e atualiza as latitudes e longitudes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $partners = Partners::whereNull('latitude')
            ->whereNull('longitude')
            ->get();
        $this->info($partners->count() . ' parceiro(s) sem latitude e longitude');


        $partners->each(function (Partners $partners){
            try {
                $partners->updLatLong();
                $this->info($partners->getKey().' atualizado');
            } catch (\Exception $e) {
                $this->error($this->getKey().' '.$e->getMessage());
            }
        });
    }
}
