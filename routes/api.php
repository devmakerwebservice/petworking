<?php

use Illuminate\Http\Request;

Route::middleware('api')->post('/login', 'Api\\AuthController@login');
Route::middleware('api')->post('/recover', 'Api\\AuthController@password');

Route::middleware('api')->resource('/user', 'Api\\UserController', ['except' => ['index', 'create', 'show', 'edit', 'destroy']]);

Route::middleware('api')->resource('/animal', 'Api\\AnimalController');
Route::middleware('api')->post('/images/{user_id}', 'Api\\AnimalController@images');
Route::middleware('api')->delete('/delete/images', 'Api\\AnimalController@deleteImages');
Route::middleware('api')->get('/myPets/{id}', 'Api\\AnimalController@myPets');

Route::middleware('api')->get('/categories', 'Api\\CategoryController@index');
Route::middleware('api')->get('/categories/{id}', 'Api\\CategoryController@find');
Route::middleware('api')->get('/banner', 'Api\\CategoryController@banner');
Route::middleware('api')->post('/anunciar', 'Api\\CategoryController@request');

Route::middleware('api')->get('/dicas', 'Api\\DicasController@index');
Route::middleware('api')->post('/dicas', 'Api\\DicasController@store');

Route::middleware('api')->get('/chats/{id}', 'Api\\ChatController@index');
Route::middleware('api')->post('/chat', 'Api\\ChatController@store');
Route::middleware('api')->get('/chat/{id}', 'Api\\ChatController@show');
Route::middleware('api')->get('/chat/delete/{id}', 'Api\\ChatController@destroy');
