<?php

Route::group(['middleware'=>'auth'], function() {
    Route::get('/', 'Admin\\AdminController@index');

    Route::get('/pet/delete/{id}', 'Admin\\PetsController@destroy');
    Route::get('/pet/view/{id}', 'Admin\\PetsController@show');
    Route::get('/cadastros', 'Admin\\PetsController@indexNew');
    Route::post('cadastros/table', ['as' => 'cadastros.table', 'uses' => 'Admin\\PetsController@tableNew']);

    Route::get('/encontrados', 'Admin\\PetsController@indexFind');
    Route::post('encontrados/table', ['as' => 'encontrados.table', 'uses' => 'Admin\\PetsController@tableFind']);

    Route::get('/lista', 'Admin\\UsersController@indexList');
    Route::post('lista/table', ['as' => 'lista.table', 'uses' => 'Admin\\UsersController@tableList']);

    Route::get('/admins', 'Admin\\UsersController@indexAdm');
    Route::post('admins/table', ['as' => 'admins.table', 'uses' => 'Admin\\UsersController@tableAdm']);
    Route::get('/admin', 'Admin\\UsersController@admin');
    Route::post('/admin/new', 'Admin\\UsersController@adminNew');

    Route::get('/parceiros', 'Admin\\PartnersController@index');
    Route::post('parceiros/table', ['as' => 'parceiros.table', 'uses' => 'Admin\\PartnersController@table']);
    Route::get('/parceiro/cadastro', 'Admin\\PartnersController@create');
    Route::post('/parceiro/new', 'Admin\\PartnersController@store');
    Route::get('/parceiro/delete/{id}', 'Admin\\PartnersController@destroy');
    Route::get('/parceiro/delete/{id}', 'Admin\\PartnersController@show');
    Route::get('/parceiro/edit/{id}', 'Admin\\PartnersController@edit');
    Route::post('/parceiro/editar/{id}', 'Admin\\PartnersController@update');
    Route::get('/parceiro/view/{id}', 'Admin\\PartnersController@show');

    Route::get('/dicas', 'Admin\\AdminController@tips');
    Route::post('dicas/table', ['as' => 'dicas.table', 'uses' => 'Admin\\AdminController@tableTip']);
    Route::get('/dica/delete/{id}', 'Admin\\AdminController@destroyTip');
    Route::get('/dica/edit/{id}', 'Admin\\AdminController@formTip');
    Route::post('/dica/editar/{id}', 'Admin\\AdminController@editTip');
    Route::get('/dica/aprov/{id}', 'Admin\\AdminController@checkTip');

    Route::get('/user', 'Admin\\UsersController@user');
    Route::post('/user/editar', 'Admin\\UsersController@edit');
    Route::get('/user/delete/{id}', 'Admin\\UsersController@destroy');

    Route::get('/notificacao', 'Admin\\AdminController@notification');
    Route::post('/notify', 'Admin\\AdminController@notify');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
